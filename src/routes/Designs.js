import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {take} from 'lodash'
import {TweenMax, Elastic} from "gsap"
import api from '../services/api'
import {Card, Spin, Modal, Icon} from 'antd'
import './Designs.css'

class Designs extends Component {

    state = {
        loading: true,
        designs: []
    }

    async componentDidMount() {
        const data = await api.getDesigns(1)
        this.setState({
            designs: take(data['DesignsCollection'], 20),
            loading: false
        })
    }

    render() {
        const {loading, designs} = this.state

        return <div className="designs">
            <div></div>
            {
                loading
                    ? <div className="spinner"><Spin size="large"/></div>
                    : <List designs={designs}/>
            }
        </div>
    }
}

class List extends Component {
    render() {
        const {designs} = this.props

        return (
            <div>
                {
                    designs.map(d =>
                        <ItemCard key={`design-${d.Id}`} item={d}/>
                    )
                }
            </div>
        )
    }
}

const duration = 0.5
const delay = 0.05
let index = 0
const gridStyle = {
    width: '25%',
    textAlign: 'center',
    opacity: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    cursor: 'pointer'
}

class ItemCard extends Component {

    state = {
        mounted: false,
        showModal: false
    }

    componentDidMount() {
        this.dom = ReactDOM.findDOMNode(this)
        this.show(this.dom, () => {
        })
    }

    show = (target, cb) => {
        index = index + delay
        return TweenMax.to(target, duration, {
            opacity: 1,
            delay: index,
            onComplete() {
                cb()
            },
            ease: Elastic.easeOut.config(0, 1)
        })
    }

    showPreview = () => {
        const {PreviewUrl} = this.props.item
        this.setState({
            imageUrl: PreviewUrl
        })
    }

    showThumbnail = () => {
        const {ThumbnailUrl} = this.props.item
        this.setState({
            imageUrl: ThumbnailUrl
        })
    }

    showModal = () => {
        this.setState({
            showModal: true
        })
    }

    hideModal = () => {
        this.setState({
            showModal: false
        })
    }

    render() {
        const {Name, ThumbnailUrl, Likes} = this.props.item
        const imageUrl = this.state.imageUrl || ThumbnailUrl

        return (
            <Card.Grid style={gridStyle} onClick={this.showModal}>
                <div className="custom-image"
                     onMouseEnter={this.showPreview}
                     onMouseLeave={this.showThumbnail}>
                    <img alt="alt" src={imageUrl} style={{width: 230, height: 160}}/>
                </div>
                <div className="custom-card">
                    <h3>{Name}</h3>
                    <p>
                        <Icon type="like-o" />
                        <span>{Likes}</span>
                    </p>
                </div>
                <Modal
                    width={1000}
                    visible={this.state.showModal}
                    wrapClassName="vertical-center-modal"
                    onCancel={this.hideModal}
                >
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                </Modal>
            </Card.Grid>
        )
    }
}

export default Designs