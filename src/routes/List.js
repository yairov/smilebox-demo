import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {TweenMax, Elastic} from 'gsap'
import createAbsoluteGrid from '../grid/AbsoluteGrid'
import {take} from 'lodash'
import {Card, Spin, Modal, Icon} from 'antd'
import api from '../services/api'
import './List.css'

const duration = 0
const delay = 0.05
const cardStyle = {
    width: 0,
    height: 0,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    cursor: 'pointer',
    opacity: 0
}

class CardItem extends React.Component {

    state = {
        mounted: false,
        showModal: false
    }

    componentDidMount() {
        this.dom = ReactDOM.findDOMNode(this)
        this.show(this.dom, () => {
        })
    }

    show = (target, cb) => {
        const {index} = this.props
        TweenMax.to(target, 0, {
            opacity: 1,
            height: 300,
            width: 300,
            delay: index * delay,
            onComplete() {
                cb()
            },
            ease: Elastic.easeOut.config(0, 1)
        })
    }

    showPreview = () => {
        const {PreviewUrl} = this.props.item
        this.setState({
            imageUrl: PreviewUrl
        })
    }

    showThumbnail = () => {
        const {ThumbnailUrl} = this.props.item
        this.setState({
            imageUrl: ThumbnailUrl
        })
    }

    showModal = () => {
        this.setState({
            showModal: true
        })
    }

    hideModal = () => {
        this.setState({
            showModal: false
        })
    }

    render() {
        const {Name, ThumbnailUrl, Likes} = this.props.item
        const imageUrl = this.state.imageUrl || ThumbnailUrl

        return <Card.Grid style={cardStyle} onClick={this.showModal}>
            <div className="custom-image"
                 onMouseEnter={this.showPreview}
                 onMouseLeave={this.showThumbnail}>
                <img alt="alt" src={imageUrl} style={{width: 230, height: 160}}/>
            </div>
            <div className="custom-card">
                <h3>{Name}</h3>
                <p>
                    <Icon type="like-o"/>
                    <span>{Likes}</span>
                </p>
            </div>
            <Modal
                width={1000}
                visible={this.state.showModal}
                wrapClassName="vertical-center-modal"
                onCancel={this.hideModal}
            >
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </Card.Grid>
    }
}

const AbsoluteGrid = createAbsoluteGrid(CardItem)


class List extends Component {

    state = {
        loading: true,
        designs: [],
        filter: null
    }

    async componentDidMount() {
        const data = await api.getDesigns(1)
        this.setState({
            designs: take(data['DesignsCollection'], 20),
            loading: false
        })
    }

    filterDesigns = async filter => {
        this.setState({
            loading: true
        })

        const data = await api.getDesigns(1, filter)
        this.setState({
            designs: take(data['DesignsCollection'], 20),
            filter,
            loading: false
        })
    }

    render() {
        const {loading, designs, filter} = this.state
        const data = designs.map((d, index) => ({...d, key: index, sort: index}))

        return (
            <div className="list">
                <div className="filters">
                    <a href="javascript:void(0)" onClick={() => this.filterDesigns('mostpopular')}>Popular</a>&nbsp;
                    |&nbsp;
                    <a href="javascript:void(0)" onClick={() => this.filterDesigns('newest')}>Newset</a>&nbsp;|&nbsp;
                    <a href="javascript:void(0)" onClick={() => this.filterDesigns('favorites')}>My
                        Preferences</a>&nbsp;|&nbsp;
                    <a href="javascript:void(0)" onClick={() => this.filterDesigns('userrecent')}>My Recent</a>
                </div>
                {
                    loading
                        ? <div className="spinner"><Spin size="large"/></div>
                        : <AbsoluteGrid
                            items={data}
                            dragEnabled={true}
                            responsive={true}
                            itemWidth={300}
                            itemHeight={300}
                        />
                }
            </div>
        )
    }
}

export default List