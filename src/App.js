import React from 'react'
import {Route, Redirect, HashRouter as Router} from 'react-router-dom'
import Designs from './routes/Designs'
import List from './routes/List'
import 'antd/dist/antd.css'
import './App.css'

const App = () => (
    <Router>
        <div>
            <header className="barfix">
                <div id="bar-section">
                    <nav className="bar-header">
                        <div className="bar-header-conatiner">
                            <div className="bar-left-side">
                                <div className="icon-wrapper bar-display">
                                    <a href="/">
                                        <img src="/img/smilebox-logo-h-white.png"/>
                                    </a>
                                </div>
                                <div className="bar-options bar-display">
                                    <ul className="bar-main-options">
                                        <li id="all-designs-menu" className="bar-main-options-li">
                                            ALL DESIGNS<span className="glyphicon-circle-arrow-down"></span>
                                            <div className="designs-list">
                                                <div className="designs-list-container">
                                                    <ul className="menu-subs">
                                                        <li><a href="#">Anytime</a></li>
                                                        <li><a href="#">Birthday</a></li>
                                                        <li><a href="#">Wedding</a></li>
                                                        <li><a href="#">Love</a></li>
                                                        <li><a href="#">Memorial</a></li>
                                                        <li><a href="#">Graduation</a></li>
                                                        <li><a href="#">Back to School</a></li>
                                                        <li><a href="#">Halloween</a></li>
                                                        <li><a href="#">Christmas</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="bar-main-options-li"><a href="/MyCreations">MY CREATIONS</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="bar-right-side">
                                <div className="bar-search">

                                    <input type="text" name="txtsearch" id="txtsearch" value=""
                                           placeholder="Search designs..." autocomplete="off"/>
                                    <div id="search-results-container">
                                        <ul></ul>
                                    </div>
                                </div>
                                <div className="bar-notification bar-display">
                                    <ul className="bar-main-notification">
                                        <li className="bell-li">
                                            <img src="/img/icons/general-toolbar-notifications-icon.svg" id="bell-img"/>
                                            <div className="bell-wrapper">
                                                <div className="bell-container">
                                                    <div className="bell-box">
                                                        <div className="bell-header">
                                                            <div className="bell-title">
                                                                <p>NOTIFICATION CENTER</p>
                                                            </div>
                                                            <div className="bell-header-links">
                                                                <ul>
                                                                    <li>
                                                                        <a href="#" id="bell-read-all">MARK ALL AS
                                                                            READ </a> |
                                                                    </li>
                                                                    <li>
                                                                        <a href="/profile/settings">SETTINGS</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="scroll-wrapper scrollbar-macosx bell-macscroll"
                                                            style={{position: 'relative'}}>
                                                            <div
                                                                className="scrollbar-macosx bell-macscroll scroll-content"
                                                            >
                                                                <div className="bell-content">
                                                                    <div
                                                                        className="bell-content-box bell-content-read">

                                                                        <div className="bell-content-empty">You have
                                                                            no new notifications!
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="scroll-element scroll-x">
                                                                <div className="scroll-element_outer">
                                                                    <div className="scroll-element_size"></div>
                                                                    <div className="scroll-element_track"></div>
                                                                    <div className="scroll-bar"></div>
                                                                </div>
                                                            </div>
                                                            <div className="scroll-element scroll-y">
                                                                <div className="scroll-element_outer">
                                                                    <div className="scroll-element_size"></div>
                                                                    <div className="scroll-element_track"></div>
                                                                    <div className="scroll-bar"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="bell-footer">
                                                            <ul>
                                                                <li><a href="#" className="cta-policy"
                                                                       data-name="about">ABOUT</a></li>
                                                                <li><a
                                                                    href="https://smilebox-plus.zendesk.com/hc/en-us"
                                                                    target="_blank">FAQ</a></li>
                                                                <li><a href="#" className="cta-policy"
                                                                       data-name="privacy">PRIVACY</a></li>
                                                                <li><a href="#" className="cta-policy"
                                                                       data-name="terms">TERMS</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="bar-account bar-display">
                                    <div>
                                        <div className="bar-name-wrapper">
                                            Hi Yair
                                        </div>
                                        <div className="bar-img-profile-wrapper">
                                            <div className="default-avatar default-avatar-small" id="bar-pf-avatar">
                                                YO
                                            </div>
                                            <div className="profile-dash-options">
                                                <div className="profile-dash-title">
                                                    <h2>Yair Oval</h2>
                                                    <h3>yairov@gmail.com</h3>
                                                </div>
                                                <div className="profile-dash-sections">
                                                    <ul>
                                                        <li>
                                                            <a href="/profile">My Profile</a>
                                                        </li>
                                                        <li>
                                                            <a href="/profile/settings">Settings</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div className="profile-dash-cta">
                                                    <div className="profile-dash-cta-signout">
                                                        <a href="/Account/Logout">Sign Out</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>

            </header>
            <Route exact path="/" render={() => <Redirect to="/list/"/>}/>
            <Route exact path="/designs" component={Designs}/>
            <Route exact path="/list" component={List}/>
            <Route exact path="/my" component={List}/>
        </div>
    </Router>
)

export default App
