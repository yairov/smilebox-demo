import querystring from 'querystring'

const getDesigns = (page, filter) => {
    const qs = querystring.stringify({
        page,
        filter: `sort*${filter}`
    })

    return json(fetch(`/Designs/Get?${qs}`))
}

const getDesignsCMS = () => {
    return json(fetch('Home/GetDesignsCMS'))
}

const getSubcategories = () => {
    return json(fetch('Subcategories/GetAll'))
}

const getByDesignId = id => {
    return json(fetch(`Templates/GetByDesignId?desingId=${id}`))
}

const search = term => {
    return json(fetch('Search/Suggestions', {method: 'POST', body: {term}}))
}

const json = func => {
    return func.then(r => r.json())
}
export default {
    getDesigns,
    getDesignsCMS,
    getSubcategories,
    getByDesignId,
    search
}